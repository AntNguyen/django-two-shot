from django.shortcuts import render,get_object_or_404,redirect
from receipts.models import Receipt,ExpenseCategory,Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceipt,CreateExpenseCategory,CreateAccount
# Create your views here.

@login_required(redirect_field_name="login")
def receipt_view(request):
    receipts = Receipt.objects.filter(purchaser = request.user)

    context = {
        "receipts":receipts
    }
    return render(request,"receipts/home.html",context)


@login_required(redirect_field_name="login")
def create_receipt(request):
    if request.method =="POST":
        form =CreateReceipt(request.POST)
        if form.is_valid():
            form = form.save(False)
            form.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = CreateReceipt()
    context = {
        "form":form
    }
    return render(request,"receipts/create.html",context)

@login_required(redirect_field_name="login")
def expense_view(request):
    expenses = ExpenseCategory.objects.filter(owner =request.user)

    context = {
        "expenses":expenses
    }
    return render(request,"receipts/expense.html",context)

@login_required(redirect_field_name="login")
def account_view(request):
    accounts = Account.objects.filter(owner =request.user)

    context = {
        "accounts":accounts
    }
    return render(request,"receipts/accounts.html",context)

@login_required(redirect_field_name="login")
def expense_create(request):
    if request.method == "POST":
        form =CreateExpenseCategory(request.POST)
        if form.is_valid():
            form = form.save(False)
            form.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = CreateExpenseCategory()
    context = {
        "form": form
    }
    return render(request,"receipts/create_expense.html",context)

@login_required(redirect_field_name="login")
def account_create(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            form =form.save(False)
            form.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form= CreateAccount()
    context = {
        "form": form
    }
    return render(request,"receipts/create_account.html",context)
