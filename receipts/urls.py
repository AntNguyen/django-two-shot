from django.urls import path
from receipts.views import receipt_view, create_receipt,expense_view,account_view,expense_create,account_create

urlpatterns = [
    path("accounts/create/",account_create,name="create_account" ),
    path("categories/create/",expense_create,name ="create_category"),
    path("accounts/",account_view,name="account_list"),
    path("categories/",expense_view,name="category_list"),
    path("",receipt_view,name = "home"),
    path("create/",create_receipt,name="create_receipt"),

]
