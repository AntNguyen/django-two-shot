from django.urls import path
from accounts.views import login_view, logout_view,sign_up_view

urlpatterns = [
    path("signup/",sign_up_view,name="signup"),
    path("login/",login_view,name="login"),
    path("logout/",logout_view,name= "logout")
]
